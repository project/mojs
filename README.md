INTRODUCTION
------------

This module integrates the 'mo.js' library
  - mojs.github.io

mo · js is a javascript motion graphics library that is a fast, retina ready,
modular and open source. In comparison to other libraries, it has a different
syntax and code animation structure approach. The declarative API provides you
a complete control over the animation, making it customizable with ease.

The library provides built-in components to start animating from scratch like
html, shape, swirl, burst and stagger, but also bring you tools to help craft
your animation in a most natural way. Using mojs on your site will enhance
the user experience, enrich your content visually and create delightful
animations precisely.


FEATURES
--------

'mo.js' library is:

  - Fast
    Silky smooth animations and effects for staggering user's experience.

  - Retina Ready
    Screen density independent effects look good on any device.

  - Simple
    Declarative APIs to master your motion project with ease.

  - Modular
    Custom build for the current project's needs. Bye bye large
    file size overheads.

  - Robust
    1580+ unit tests and ci techniques help us to carve & backstrap
    the reliable tool.

  - Open Sourced
    Great growing community refines mo· js fast and ships frequently.


REQUIREMENTS
------------

You don't need to download library, Already included.


INSTALLATION
------------

1. Download 'MO.JS' module - https://www.drupal.org/project/mojs

2. Extract and place it in the root of contributed modules directory i.e.
   /modules/contrib/mojs

3. Now, enable 'MO.JS' module


USAGE
-----

It’s very simple to use a library, which can be downloaded as a one CSS file
and added to your project to use one of many predefined animations by adding
a class to an element.

You can customize selected animations by setting the delay and speed
of the effect. It’s possible to use animations with pure HTML and CSS projects,
but you can also implement Javascript as well.

BASIC USAGE
===========

Check mo.js official tutorials
  - https://mojs.github.io/tutorials/


MAINTAINERS
-----------

Current module maintainer:

 * Mahyar Sabeti - https://www.drupal.org/u/mahyarsbt


DEMO
----
https://mojs.github.io/
